# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Márcio Sousa Rocha <marciosr10@gmail.com>
# Baseado no PKGBUILD feito por Lara Maia <lara@craft.net.br>
 
_date=2017
_pkgname=irpf
pkgname=${_pkgname}${_date}
java=7
_pkgver=1.4
pkgver=${_date}.${_pkgver}
pkgrel=1.2
license=('custom')
arch=(any)
pkgdesc='Programa Oficial da Receita para elaboração do IRPF'
url='https://www.receita.fazenda.gov.br'
makedepends=('imagemagick')
install=${_pkgname}.install
source=(https://downloadirpf.receita.fazenda.gov.br/irpf/${_date}/irpf/arquivos/IRPF${pkgver/./-}.zip
        "https://metainfo.manjariando.com.br/${_pkgname}/gov.${_pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/receita-federal.png"
        "${_pkgname}"
        "Copyright")
sha256sums=('cd97476a01b4b7fedbc5ccba1cdf24953d68222460b178b3304e7e95a9cbe79b'
            '6ba9e47e9e9f02ced724bf176fd106eecf0f9baf8ffe49c21a719c2d5dad2ae2'
            '541a33893db3730e4d858d5656de055b957801ad958f827b6cd30f308f663684'
            '3192efcab4d81b76707f76a2b2df91ee13f4dd4fe67ec622dde4f26455b8a9a0'
            'ed5e59cc21aa55c62ccebd6742dfb4f4ad7c271578cb81c89c42af768de41b81')

_irpf_desktop="[Desktop Entry]
Name=Imposto de Renda Pessoa Física ${_date}
Comment=Programa Oficial da Receita para elaboração do IRPF ${_date}
Exec=${pkgname}
StartupNotify=true
Icon=${pkgname}
Terminal=false
Type=Application
Categories=Network;"

build() {
    cd "${srcdir}"
    echo -e "$_irpf_desktop" | tee gov.${_pkgname}.desktop
}
 
package() {
    depends=("java-runtime=${java}" 'hicolor-icon-theme' 'sh')

    cd "${srcdir}"/IRPF${_date}
    mkdir -p "${pkgdir}"/{usr/bin,usr/share/${pkgname}}
       
    cp -rf tutorial "${pkgdir}"/usr/share/${pkgname}/
    cp -rf lib "${pkgdir}"/usr/share/${pkgname}/
    cp -rf help "${pkgdir}"/usr/share/${pkgname}/
      
    install -Dm755 irpf.jar "${pkgdir}"/usr/share/${pkgname}/${pkgname}.jar

    install -Dm644 Leia-me.htm "${pkgdir}"/usr/share/${pkgname}/
    install -Dm644 offline.png "${pkgdir}"/usr/share/${pkgname}/
    install -Dm644 online.png "${pkgdir}"/usr/share/${pkgname}/
    install -Dm644 pgd-updater.jar "${pkgdir}"/usr/share/${pkgname}/

    install -Dm755 "${srcdir}/irpf" "${pkgdir}/usr/bin/${pkgname}"
       
    # Appstream
    install -Dm644 "${srcdir}/gov.${_pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/gov.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/Copyright" "${pkgdir}/usr/share/licenses/${pkgname}/Copyright"
    install -Dm644 "${srcdir}/gov.${_pkgname}.desktop" "${pkgdir}/usr/share/applications/gov.${pkgname}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/receita-federal.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done

    sed -i "s:JAVA=.*:JAVA=${java}:" "${startdir}/${_pkgname}.install"
    sed -i ":icon: s:irpf:${pkgname}:" "${pkgdir}/usr/share/metainfo/gov.${pkgname}.metainfo.xml"
    sed -i "s:IRPF:IRPF ${_date}:" "${pkgdir}/usr/share/metainfo/gov.${pkgname}.metainfo.xml"
}
